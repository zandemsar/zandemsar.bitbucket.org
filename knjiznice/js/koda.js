
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";

var sporocilo1="Pacient naj izgubi kile in naj dnevno zaužije približno 300 kalorij manj kot je vrednost njegovega BMR. Poveča naj tudi količino gibanja.";
var sporocilo3="Pacient naj pridobi težo in naj dnevno zaužije toliko kalorij kakor znaša njegov BMR. V primeru, da bi povečal količino gibanja naj poveča tudi vnos kalorij.";
var sporocilo2="Pacient naj vzdržuje svojo telesno težo pa naj zaužije 100 kalorij manj kolikor znaša njegov BMR. Gibanje naj ohrani na istem nivoju kot je.";


/**
 * Generiranje niza za avtorizacijo na podlagi uporabniškega imena in gesla,
 * ki je šifriran v niz oblike Base64
 *
 * @return avtorizacijski niz za dostop do funkcionalnost
 */
function getAuthorization() {
  return "Basic " + btoa(username + ":" + password);
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
 
function generirajPodatke(stPacienta) {
  ehrId = "";
  
 
  
  
  var stPacienta=imePacienta();
  var stMere = stMeritve();
  var racun;
  var ITM;
  
 
  
  // TODO: Potrebno implementirati

if(stPacienta==1) {
  
    var pacient1 = 
    {
    "stPacienta":"1",
    "Name":"Luka",
    "Surname":"Novak",
    "Sex":"male",
    "Meritve":["90","85","95","82","81"],
    "Height":"1.9",
    "Age":"55",
    "sportHobbies":["Golf","Basketball","Football","Baseball"],
    "sportTimePerDay":"30",
    }
    document.getElementById("teza").value=pacient1.Meritve[stMere-1];
    document.getElementById("starost").value=pacient1.Age;
    document.getElementById("visina").value=pacient1.Height;
    

  if(pacient1.sportHobbies.length>3 && pacient1.sportTimePerDay>40) {
    document.getElementById("gibanje").value="zelo aktivno";
    racun =Math.round(1,8 * (8,3*pacient1.Meritve[stMere-1] + 846));
    document.getElementById("BMR").value=racun;
    document.getElementById("message").innerHTML=sporocilo2;
  }
  
  else if(pacient1.sportHobbies.length>1 && pacient1.sportTimePerDay>20) {
    document.getElementById("gibanje").value="aktivno";
    racun =Math.round(1.6 * (8.3*pacient1.Meritve[stMere-1] + 846));
    document.getElementById("BMR").value=racun;
    document.getElementById("message").innerHTML=sporocilo3;
  }
  
  else if(pacient1.sportHobbies.length>-1 && pacient1.sportTimePerDay>0) {
    document.getElementById("gibanje").value="neaktivno";
    racun =Math.round(1,4 * (8,3*pacient1.Mertive[stMere-1] + 846));
    document.getElementById("BMR").value=racun;
    document.getElementById("message").innerHTML=sporocilo1;
  }
  

}
  
else if(stPacienta==2) {
  
  var pacient2 = 
  {
    "stPacienta":"2",
    "Name":"Berta",
    "Surname":"Rozic",
    "Sex":"female",
    "Meritve":["75","65","60","68","73"],
    "Height":"1,55",
    "Age":"25",
    "sportHobbies":["Walking"],
    "sportTimePerDay":"5",
  }
  document.getElementById("teza").value=pacient2.Meritve[stMere-1];
  document.getElementById("starost").value=pacient2.Age;
  document.getElementById("visina").value=pacient2.Height;
  
  if(pacient2.sportHobbies.length>3 && pacient2.sportTimePerDay>40) {
    document.getElementById("gibanje").value="zelo aktivno";
    racun =Math.round(1.8 * (14.8*pacient2.Meritve[stMere-1] + 487));
    document.getElementById("BMR").value=racun;
    document.getElementById("message").innerHTML=sporocilo2;
  }
  
  else if(pacient2.sportHobbies.length>1 && pacient2.sportTimePerDay>20) {
    document.getElementById("gibanje").value="aktivno";
    racun =Math.round(1.6 * (14.8*pacient2.Meritve[stMere-1] + 487));
    document.getElementById("BMR").value=racun;
    document.getElementById("message").innerHTML=sporocilo3;
  }
  
  else if(pacient2.sportHobbies.length>-1 && pacient2.sportTimePerDay>0) {
    document.getElementById("gibanje").value="neaktivno";
    racun =Math.round(1.4 * (14.8*pacient2.Meritve[stMere-1] + 487));
    document.getElementById("BMR").value=racun;
    document.getElementById("message").innerHTML=sporocilo1;
  }
  
}

else if(stPacienta==3) {
  
  var pacient3 = 
  {
    "stPacienta":"3",
    "Name":"Janez",
    "Surname":"Kosir",
    "Sex":"male",
    "Meritve":["80","75","83","77","88"],
    "Height":"2,0",
    "Age":"17",
    "sportHobbies":["Hiking","Skiing","Running","Swimming","Fitness","Cycling"],
    "sportTimePerDay":"180"
  }
  document.getElementById("teza").value=pacient3.Meritve[stMere-1];
  document.getElementById("starost").value=pacient3.Age;
  document.getElementById("visina").value=pacient3.Height;
  
 if(pacient3.sportHobbies.length>3 && pacient3.sportTimePerDay>40) {
    document.getElementById("gibanje").value="veliko";
    racun =Math.round(1.8 * (13.4*pacient3.Meritve[stMere-1] + 692));
    document.getElementById("BMR").value=racun;
    document.getElementById("message").innerHTML=sporocilo2;
  }
  
  else if(pacient3.sportHobbies.length>1 && pacient3.sportTimePerDay>20) {
    document.getElementById("gibanje").value="srednje";
    racun =Math.round(1.6 * (13.4*pacient3.Meritve[stMere-1] + 692));
    document.getElementById("BMR").value=racun;
    document.getElementById("message").innerHTML=sporocilo3;
  }
  
  else if(pacient3.sportHobbies.length>-1 && pacient3.sportTimePerDay>0) {
    document.getElementById("gibanje").value="malo";
    racun =Math.round(1.4 * (13.4*pacient3.Meritve[stMere-1] + 692));
    document.getElementById("BMR").value=racun;
    document.getElementById("message").innerHTML=sporocilo1;
  }
}


  return ehrId;
}


function kreirajEHRzaBolnika() {
	var ime = $("#kreirajIme").val();
	var priimek = $("#kreirajPriimek").val();
  var datumRojstva = $("#kreirajDatumRojstva").val();

	if (!ime || !priimek || !datumRojstva || ime.trim().length == 0 ||
      priimek.trim().length == 0 || datumRojstva.trim().length == 0) {
		$("#kreirajSporocilo").html("<span class='obvestilo label " +
      "label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		$.ajax({
      url: baseUrl + "/ehr",
      type: 'POST',
      headers: {
        "Authorization": getAuthorization()
      },
      success: function (data) {
        var ehrId = data.ehrId;
        var partyData = {
          firstNames: ime,
          lastNames: priimek,
          dateOfBirth: datumRojstva,
          additionalInfo: {"ehrId": ehrId}
        };
        $.ajax({
          url: baseUrl + "/demographics/party",
          type: 'POST',
          headers: {
            "Authorization": getAuthorization()
          },
          contentType: 'application/json',
          data: JSON.stringify(partyData),
          success: function (party) {
            if (party.action == 'CREATE') {
              $("#kreirajSporocilo").html("<span class='obvestilo " +
                "label label-success fade-in'>Uspešno kreiran EHR '" +
                ehrId + "'.</span>");
              $("#preberiEHRid").val(ehrId);
            }
          },
          error: function(err) {
          	$("#kreirajSporocilo").html("<span class='obvestilo label " +
              "label-danger fade-in'>Napaka '" +
              JSON.parse(err.responseText).userMessage + "'!");
          }
        });
      }
		});
	}
}
// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija
